#include"seeed_line_chart.h" //Library used to display the line chart
#include "TFT_eSPI.h" //TFT LCD library 
#include "seeed_bme680.h"

#define IIC_ADDR  uint8_t(0x76)
Seeed_BME680 bme680(IIC_ADDR);

TFT_eSPI tft;

doubles data; //Initialising a doubles type to store data
TFT_eSprite spr = TFT_eSprite(&tft);  //Function for the initialisation of the sprites on the LCD


int dat[2000], timer, m, w, y, i, mod;

float V[4];

void setup() {
    
//Initialisation of the LCD sprites 
    tft.begin();
    spr.setRotation(3);
    tft.setRotation(3);
    spr.createSprite (TFT_HEIGHT, TFT_WIDTH);

//Initialisation of Gas sensor
bme680.init();

//Setting up initial variables    

    mod = 1;
    y = 0;
    timer = 0;
}

void loop() {

//First, we read the values from the sensor, and we store them

bme680.read_sensor_data();

V[2] = bme680.sensor_result_value.temperature;
V[1] = bme680.sensor_result_value.humidity;
V[0] = bme680.sensor_result_value.gas/1000;
    
//Settings for the screen headings
  spr.fillSprite(TFT_WHITE); 
  spr.fillRect(0, 0, 320, 50, TFT_NAVY);
  spr.fillRect(0, 50, 120, 200, TFT_BLUE);
  spr.setTextColor (TFT_WHITE);
  spr.setTextSize (3);
  spr.drawString ("Soil Sensor", 40, 15);
  spr.setTextSize (2);

//Functions to display the data on the LCD
  spr.setTextSize (2);
  spr.setTextColor (TFT_WHITE);
  spr.drawString("Temp",10,90); 
  spr.drawNumber(V[2], 60, 90);
  spr.drawString("Hum.",10,120); 
  spr.drawNumber(V[1], 60, 120);
  spr.drawString("VOC",10,180); 
  spr.drawNumber(V[0], 60, 180);
  spr.setTextColor (TFT_BLACK);

//Settings for the line graph title
  auto header =  text(187, 57)

                .value("VOC")
                .width(9)
                .height (4);

    header.height(9);
    header.draw();

//We draw 2 lines to complete the graph square, but only when the graph is first made
  if (w == 2){    
  spr.drawFastVLine(312,73,150,TFT_BLACK);
  spr.drawFastVLine(223,215,5,TFT_BLACK);
  spr.drawFastVLine(267,215,5,TFT_BLACK);
  spr.drawFastHLine(179,73,133,TFT_BLACK);

     
//Settings for displaying the line graph
   auto content = line_chart(152, 70); //(x,y) where the line graph begins
   content
             .height(160) //actual height of the line chart
             .width(160) //actual width of the line chart
             .based_on(dat[0]) //Starting point of y-axis, must be a float
             .show_circle(true) //drawing a cirle at each point, default is on.
             .value(data) //passing through the data to line graph
             .color(TFT_RED) //Setting the color for the line
             .draw();
}


    spr.pushSprite(0, 0);
    delay(100);
    w = 2;


//This function populates the graph displayed on the LCD screen and remove end points
   data.push(V[0]); 

//This indicates how many points are displayed on the graphs
    if (data.size() == 60) {
        data.pop(); //this is used to remove the first read variable
    }  

//The system can operate by displaying data every min or every second, depending on the value of mod
    if (mod == 1){
    if (timer>1) {
    m = 3;
    timer = 0;
    y++;
    if (y>60){
      y = 60;
    }
   }
    }

    if (mod == 0){
    if (timer>120) {
    m = 3;
    timer = 0;
    y++;
    if (y>60){
      y = 60;
    }
   }
    }

 //The program updates every 0.5 s
   delay (500);
   timer++;
}
